const APIURL =
  "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=04c35731a5ee918f014970082a0088b1&page=1";
const IMGPATH = "https://image.tmdb.org/t/p/w1280";
const SEARCHAPI =
  "https://api.themoviedb.org/3/search/movie?&api_key=04c35731a5ee918f014970082a0088b1&query=";

const main = document.getElementById("main");
const form = document.getElementById("form");
const search = document.getElementById("search");
const chosenMovie = document.getElementById("chosen-movie");
const secondPage = document.getElementById("second-page");
const chosenMovieImg = document.getElementById("movie-id");
const movieTitle = document.getElementById("title");
const releseDate = document.getElementById("relese-date");
const overview = document.getElementById("overview");
const backgroundImg = document.getElementById("movie-background-img");
///sigin form and auth ///
const signinForm = document.getElementById("signin-form");
const signinText = document.getElementById("text");
const signinPassword = document.getElementById("password");
const logoutButton = document.getElementById("logout");
///login form and auth ///
const loginForm = document.getElementById("login-form");
const loginText = document.getElementById("login-text");
const loginPassword = document.getElementById("login-password");
const errorMessage = document.getElementById("error");
const loginErrorMessage = document.getElementById("login-error");
const userName = document.getElementById("user-name");

///firebase config///

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB9DqNpXiRNsiRS9aa0qKyYPt9nIgWKPZM",
  authDomain: "movie-app-e943d.firebaseapp.com",
  databaseURL: "https://movie-app-e943d.firebaseio.com",
  projectId: "movie-app-e943d",
  storageBucket: "movie-app-e943d.appspot.com",
  messagingSenderId: "654866569235",
  appId: "1:654866569235:web:e828e23732f3c0d4d487e6",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

const signInHendler = (e) => {
  e.preventDefault();
  console.log("submit");
  const email = signinText.value;
  const password = signinPassword.value;
  auth.createUserWithEmailAndPassword(email, password).then((data) => {
    console.log(data.user);
    signinForm.reset();
  })
  .catch((error) => {
    console.log(error.message);
    errorMessage.style.display = 'block'
    errorMessage.innerHTML = `
    <p>${error.message}</p>
    `
    setTimeout(() => {
      errorMessage.style.display = 'none'
    }, 3000);
  });

};
const loginInHendler = (e) => {
  e.preventDefault();
  console.log("login");
  const email = loginText.value;
  const password = loginPassword.value;
  auth.signInWithEmailAndPassword(email, password).then((data) => {
    console.log(data.email);
    loginForm.reset();
  }).catch((error) => {
    console.log(error.message);
    loginErrorMessage.style.display = 'block'
    loginErrorMessage.innerHTML = `
    <p>${error.message}</p>
    `
    setTimeout(() => {
      loginErrorMessage.style.display = 'none'
    }, 3000);
  });
};

signinForm.addEventListener("submit", signInHendler);
loginForm.addEventListener("submit", loginInHendler);

auth.onAuthStateChanged((user) => {
  if (user) {
    $.mobile.changePage("#first-page");
    userName.innerText = user.email
    console.log(user);
  }
});

logoutButton.addEventListener("click", (e) => {
  e.preventDefault();
  auth.signOut().then(() => {
    $.mobile.changePage("#signin-page");
    console.log(user);
    console.log("user logout");
  });
});

function setBackDefault(params) {
  getMovies(APIURL);
}
///set and get movies functions//
getMovies(APIURL);

async function getMovies(url) {
  const response = await fetch(url);
  const data = await response.json();
  setMovies(data.results);
  return data;
}

function setMovies(movieData) {
  main.innerHTML = "";
  movieData.forEach((movies) => {
    const movieContainer = document.createElement("div");
    movieContainer.classList.add("movie");
    movieContainer.innerHTML = `
        <a href="#second-page">
        <img src="${IMGPATH + movies.poster_path}" alt="${movies.title}">
        </a>
        <div class="movie-info">
        <h5 class="movie-title">${movies.title}</h5>
        <span class="${classVote(movies.vote_average)}">${
      movies.vote_average
    }</span>
        </div>
        <div class="overview">
        <h3>Overview:</h3>
        ${movies.overview}
        </div>
    `;
    main.appendChild(movieContainer);
    movieContainer.addEventListener("click", () => {
      sessionStorage.setItem(
        "movieImageSrc",
        `${IMGPATH + movies.poster_path}`
      );
      sessionStorage.setItem("movieTitle", `${movies.title}`);
      sessionStorage.setItem("releseDate", `${movies.release_date}`);
      sessionStorage.setItem("overview", `${movies.overview}`);
      sessionStorage.setItem(
        "backgroundImg",
        `${IMGPATH + movies.backdrop_path}`
      );
    });
  });
}
//form submit function//
form.addEventListener("submit", (e) => {
  e.preventDefault();
  const searchValue = search.value;
  sessionStorage.setItem("searchValue", searchValue);
  if (searchValue) {
    getMovies(SEARCHAPI + searchValue);
    search.value = "";
  }
});

//second page before show event
$("#second-page").on("pagebeforeshow", function () {
  ////get inner element from sessionStorage///

  let StorageMovieImageSrc = sessionStorage.getItem("movieImageSrc");
  let StorageMovieTitle = sessionStorage.getItem("movieTitle");
  let StorageMovieReleseDate = sessionStorage.getItem("releseDate");
  let StorageMovieOverview = sessionStorage.getItem("overview");
  let StorageMovieBackgroundImg = sessionStorage.getItem("backgroundImg");

  ////set inner element from sessionStorage///

  chosenMovieImg.src = StorageMovieImageSrc;
  secondPage.style.backgroundImage = `url(${StorageMovieBackgroundImg})`;
  movieTitle.innerText = StorageMovieTitle;
  releseDate.innerText = StorageMovieReleseDate;
  overview.innerText = StorageMovieOverview;
  console.log("second page before show event");
});

function classVote(vote) {
  if (vote >= 8) {
    return "green";
  } else if (vote >= 5) {
    return "orange";
  } else {
    return "red";
  }
}
